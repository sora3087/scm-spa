import { Component } from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import App from './App';
import { RoutedGamePage } from './routes/game/game';
import { NavigatedConnectedGameListPage } from './routes/gamelist/gamelist';
import { NotFound } from './routes/NotFound';
import { ConnectedSearchPageWithParams } from './routes/search/search';
import { RoutedSongPage } from './routes/song/Song';

type State = {
  domain: string
}

export class AppRouter extends Component<{}, State> {
  constructor(props: any) {
    super(props)

    this.state = {
      domain: ""
    }
  }
  componentWillMount () {
    let { domain } = this.state;
    const parsedData = window.location.pathname.split("/"); 
    domain = parsedData[1];

    this.setState({ domain })
  }
  render() {
    const { domain } = this.state
    return (
      <BrowserRouter
        basename={"/"+domain}
      >
        <Routes>
          <Route path="/" element={<App />}>
            <Route path="/" element={<NavigatedConnectedGameListPage />}>
              <Route path=":selected" element={<NavigatedConnectedGameListPage />} />
            </Route>
            <Route path="game">
              <Route path=":gameId" element={<RoutedGamePage />} />
            </Route>
            <Route path="song">
              <Route path=":songId" element={<RoutedSongPage />} />
            </Route>
            <Route path="search">
              <Route path=":query" element={<ConnectedSearchPageWithParams />} />
            </Route>
          </Route>
          {/* <Route path="preferences" element={<App/>} />
          <Route path="about" element={<App/>} />
          <Route path="contact" element={<App/>} />
          <Route path="tools" element={<App/>} /> */}
          <Route path="*" element={<NotFound />} />
        </Routes>
      </BrowserRouter>
    )
  }
}