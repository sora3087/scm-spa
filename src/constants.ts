export const ORIGIN = 'https://smashcustommusic.net'
export const API_PATH = '/json'

export const STREAM_TYPES = {
  brstm: "BRSTM",
  bcstm: "BCSTM",
  bfstm: "BFSTM (Wii U)",
  sw_bfstm: "BFSTM (Switch)",
  wav: "WAV",
  bwav: "BWAV",
  // nus3audio: "NUS3Audio"
}
