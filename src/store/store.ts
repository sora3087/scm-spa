import { configureStore } from '@reduxjs/toolkit'
import { appSlice } from './app/appSlice'
import { collectionSlice } from './collection/collectionSlice'
import { playerSlice } from './player/playerSlice'

export const store = configureStore({
  reducer: {
    app: appSlice.reducer,
    player: playerSlice.reducer,
    collection: collectionSlice.reducer
  }
})

export type RootState = ReturnType<typeof store.getState>

export type AppDispatch = typeof store.dispatch