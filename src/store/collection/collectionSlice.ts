import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { unionBy } from 'lodash';

type CollectionState = {
  songs: {id: number, name: string}[]
}

const initialState: CollectionState = {
  songs: []
}

export const collectionSlice = createSlice({
  name: 'collection',
  initialState,
  reducers: {
    addSong: (state, action: PayloadAction<{id: number, name: string}>) => {
      state.songs = unionBy(state.songs, [action.payload], 'id')
    },
    removeSong: (state, action: PayloadAction<number>) => {
      state.songs = state.songs.filter(song => song.id !== action.payload)
    },
    clear: () => initialState
  }
})

export const {
  addSong, removeSong, clear
} = collectionSlice.actions