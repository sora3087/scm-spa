import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { AnyAction } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { API_PATH, ORIGIN } from '../../constants';
import { SongResponse } from '../../services/SCM';

type PlayerState = {
  loading: boolean
  playing: boolean
  id: number
  loaded: number
  progress: number
  song: Pick<SongResponse, 'name' | 'size'>
}

const initialState: PlayerState = {
  loading: false,
  playing: false,
  id: -1,
  loaded: 0,
  progress: 0,
  song: {
    name: '',
    size: '0',
  }
}


export let songBuffer: ArrayBuffer = new ArrayBuffer(0)

export const getSongBuffer = () => songBuffer

class LogStreamSink implements UnderlyingSink<Uint8Array> {
  constructor(
    private dispatch: ThunkDispatch<unknown, unknown, AnyAction>
  ){}
  write(chunk: Uint8Array) {
    this.dispatch(loaded(chunk.byteLength))
  }
}

function updateProgress(
  dispatch: ThunkDispatch<unknown, unknown, AnyAction>,
  rs: ReadableStream | null
) {
  if (rs) {
    const [rs1, rs2] = rs.tee();
    rs2.pipeTo(new WritableStream(new LogStreamSink(dispatch)))
    return rs1
  }
}

export const loadSong = createAsyncThunk(
  'loadSong',
  async (songId: number, { dispatch, rejectWithValue }) => {
    // dispatch(setLoading(song))
    const song: SongResponse = await fetch(ORIGIN+API_PATH+'/song/'+songId+"?noIncrement=0")
      .then(resp => resp.json())
    if (song !== undefined) {
      dispatch(setSong([song, songId]))
      // failed to load song info
      await fetch(`${ORIGIN}/brstm/${songId}`)
      .then(resp => resp.body)
      .then(rs => updateProgress(dispatch, rs))
    }
    return
  }
)

export const playerSlice = createSlice({
  name: 'player',
  initialState,
  reducers: {
    play: (state) => {
      state.playing = true
    },
    pause: (state) => {
      state.playing = false
    },
    toggle: (state) => {
      state.playing = !state.playing
    },
    loaded: (state, action: PayloadAction<number>) => {
      state.loaded += action.payload / parseInt(state.song.size)
    },
    setSong(state, action: PayloadAction<[SongResponse, number]>){
      state.loaded = 0
      state.loading = true
      state.song = action.payload[0]
      state.id = action.payload[1]
    }
  },
  extraReducers: (builder) => {
    builder.addCase(
      loadSong.fulfilled,
      (state) => {
        state.loading = false
      }
    )
  }
})

export const {
  play, pause, toggle, loaded, setSong
} = playerSlice.actions