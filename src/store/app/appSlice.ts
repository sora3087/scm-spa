import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { groupBy } from 'lodash';
import { GameListEntry } from '../../services/SCM';

type AppState = {
  games: GameListEntry[]
  gamesByLetter: Record<string, GameListEntry[]>
}

const initialState: AppState = {
  games: [],
  gamesByLetter: {}
}

export const appSlice = createSlice({
  name: 'app',
  initialState,
  reducers: {
    setGames: (state, action: PayloadAction<GameListEntry[]>) => {
      state.games = action.payload
      state.gamesByLetter = groupBy(state.games, (game) => {
        const char = game.game_name[0].toLowerCase();
        if(/[a-z]/.test(char)){
          return char
        }
        return "#"
      })
    }
  }
})

export const {
  setGames
} = appSlice.actions