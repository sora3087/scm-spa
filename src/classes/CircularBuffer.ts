export class CircularBuffer {
  private buffer: Uint8Array
  private _high: number = 0
  private head: number = 0
  private tail: number = 0
  constructor(){
    this.buffer = new Uint8Array(0)
  }
  get free(){
    return this.buffer.length - this._high
  }
  get high(){
    return this._high
  }
  enq(data: Uint8Array) {
    if(data.length > this.free) {
      this.resize(this.buffer.length + (data.length - this.free))
    }
    for(let i = 0; i < data.length; i++){
      this.buffer[(i + this.tail) % this.buffer.length] = data[i]
    }
    this._high += data.length
    this.tail = (this.tail + data.length) % this.buffer.length
  }
  deq(length: number, peek = false){
    if (length > this._high) {
      return null
    }
    const ret = new Uint8Array(length)
    for(let i = 0; i < length; i++){
      ret[i] = this.buffer[(i + this.head) % this.buffer.length]
    }
    this._high -= length
    this.head = (this.head + length) % this.buffer.length
    return ret
  }
  drain(){
    return this.deq(this._high) || new Uint8Array()
  }
  peek(length: number){
    return this.deq(length, true)
  }
  private resize(newSize: number){
    const newBuf = new Uint8Array(newSize);

    const curBuf = this.drain()
    newBuf.set(curBuf, 0)
    
    this.head = 0
    this.tail = curBuf.length
    this.buffer = newBuf
  }
}