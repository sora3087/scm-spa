import axios, { AxiosInstance, AxiosRequestConfig, AxiosResponse } from "axios";
import { catchError, from, map, Observable, of } from "rxjs";
// import { store } from "../store/store";
// import { ResponseModel } from '../types/ResponseModel';

export type AxiosErrorResponse = AxiosResponse & { data: { Message: string } }

export type BaseResponse = {
  ok: boolean
}

export class HttpClient {
  private client: AxiosInstance;
  constructor(private baseURL: string) {
    this.client = axios.create({
      baseURL,
      timeout: 60 * 1000
    })
    // store.subscribe(() => {
    //   const token = store.getState().user.token
    //   if (this.token !== token) {
    //     this.token = token
    //     this.setToken(token)
    //   }
    // })
  }
  public setToken(token: string): void {
    this.client.defaults.headers.common = {
      ...this.client.defaults.headers.common,
      Authorization: "Bearer " + token
    }
  }
  public get<ResponseType>(url: string, config?: AxiosRequestConfig):
  Observable<ResponseType> {
    const newConfig: AxiosRequestConfig = config || {}
    return this.execute<ResponseType>(url, {
      ...newConfig,
      method: 'GET'
    })
    .pipe(
      map(resp => resp.data)
    )
  }
  public getArray<ResponseType>(url: string): Observable<ResponseType[]> {
    return this.execute<ResponseType[]>(url, {
      method: 'GET'
    })
    .pipe(
      map(resp => resp.data),
      catchError(() =>  of([]))
    )
  }
  public post<ResponseType>(url: string, config?: AxiosRequestConfig):
  Observable<ResponseType> {
    const newConfig: AxiosRequestConfig = config || {}
    return this.execute<ResponseType>(url, {
      ...newConfig,
      method: 'POST'
    })
    .pipe(
      map(resp => resp.data)
    )
  }
  public postBoolResonse(url: string, config?: AxiosRequestConfig): Observable<boolean> {
    const newConfig: AxiosRequestConfig = config || {}
    return this.executeBoolResponse(url, {
      ...newConfig,
      method: 'POST'
    })
  }
  public put<ResponseType>(url: string, config?: AxiosRequestConfig):
  Observable<ResponseType> {
    const newConfig: AxiosRequestConfig = config || {}
    return this.execute<ResponseType>(url, {
      ...newConfig,
      method: 'PUT'
    })
    .pipe(
      map(resp => resp.data)
    )
  }
  public putBoolResonse(url: string, config?: AxiosRequestConfig): Observable<boolean> {
    const newConfig: AxiosRequestConfig = config || {}
    return this.executeBoolResponse(url, {
      ...newConfig,
      method: 'PUT'
    })
  }
  public delete<ResponseType>(url: string, config?: AxiosRequestConfig):
  Observable<ResponseType> {
    const newConfig: AxiosRequestConfig = config || {}
    return this.execute<ResponseType>(url, {
      ...newConfig,
      method: 'DELETE'
    })
    .pipe(
      map(resp => resp.data)
    )
  }
  public deleteBoolResonse(url: string, config?: AxiosRequestConfig): Observable<boolean> {
    const newConfig: AxiosRequestConfig = config || {}
    return this.executeBoolResponse(url, {
      ...newConfig,
      method: 'DELETE'
    })
  }
  public executeArrayResponse<ResponseType>(url: string, config: AxiosRequestConfig): Observable<ResponseType[]> {
    return this.execute<ResponseType[]>(url, config)
    .pipe(
      map(resp => resp.data),
      catchError(() => of([]))
    )
  }
  public executeBoolResponse<ResponseType>(url: string, config: AxiosRequestConfig): Observable<boolean> {
    return this.execute<ResponseType[]>(url, config)
    .pipe(
      map(resp => (resp.status >= 200 && resp.status < 400)),
      catchError(() => of(false))
    )
  }
  public execute<ResponseType>(url: string, config: AxiosRequestConfig):
  Observable<
    AxiosResponse<
      BaseResponse & ResponseType
    >
  > {
    return from(
      this.client(url, config)
      .catch(reason => {
        if (reason.response) {
          return Promise.reject(reason.response)
        }
        return Promise.reject({
          data: { Message: "Undefined error" }
        }) as Promise<AxiosErrorResponse>
      })
    )
  }
}
