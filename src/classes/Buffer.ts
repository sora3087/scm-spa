import { Endianness } from '../enum/Endianness'

export class Buffer extends Uint8Array {
  public readFloatBE(offset = 0): number {
    return (new Float32Array(this.slice(offset, offset + 4).buffer)[0])
  }
  public readFloatLE(offset = 0): number {
    return (new Float32Array(this.slice(offset, offset + 4).reverse().buffer)[0])
  }
  public readInt8(offset = 0): number {
    return (new Int8Array(this.slice(offset))[0])
  }
  public readInt16BE(offset = 0): number {
    return new Int16Array(this.slice(offset, offset + 6).reverse().buffer)[0]
  }
  public readInt16LE(offset = 0): number {
    return new Int16Array(this.slice(offset, offset + 6).buffer)[0]
  }
  public readInt16(offset= 0, endianness: Endianness): number {
    if(endianness === 0){
      return this.readInt16LE(offset)
    }
    return this.readInt16BE(offset)
  }
  public readInt32BE(offset = 0): number {
    return new Int32Array(this.slice(offset, offset + 4).reverse().buffer)[0]
  }
  public readInt32LE(offset = 0): number {
    return new Int32Array(this.slice(offset, offset + 4).buffer)[0]
  }
  public readInt32(offset= 0, endianness: Endianness): number {
    if(endianness === 0){
      return this.readInt32LE(offset)
    }
    return this.readInt32BE(offset)
  }
  public readUInt8(offset?: number): number {
    return this[offset || 0]
  }
  public readUInt16BE(offset = 0): number {
    return new Uint16Array(this.slice(offset, offset + 2).reverse().buffer)[0]
  }
  public readUInt16LE(offset = 0): number {
    return new Uint16Array(this.slice(offset, offset + 2).buffer)[0]
  }
  public readUInt16(offset = 0, endianness: Endianness): number {
    if(endianness === 0){
      return this.readUInt16LE(offset)
    }
    return this.readUInt16BE(offset)
  }
  public readUInt32BE(offset = 0): number {
    return new Uint32Array(this.slice(offset, offset + 4).reverse().buffer)[0]
  }
  public readUInt32LE(offset = 0): number {
    return new Uint32Array(this.slice(offset, offset + 4).buffer)[0]
  }
  public readUInt32(offset= 0, endianness: Endianness): number {
    if(endianness === 0){
      return this.readUInt32LE(offset)
    }
    return this.readUInt32BE(offset)
  }
}