export const downloadBlob = (blob: Blob, fileName: string) => {
  var a = document.createElement("a");
  a.style.display = "none";
  document.body.appendChild(a);
  // return function (data, fileName) {
    var url = window.URL.createObjectURL(blob);
    a.href = url;
    a.download = fileName;
    a.click();
    window.URL.revokeObjectURL(url);
  // };
}