import moment from 'moment'

export const formatSeconds = (s: number) => {
  if (!s) return ''
  return moment.utc(s*1000).format('m:ss')
}