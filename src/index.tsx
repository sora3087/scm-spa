import { render } from 'react-dom';
import { Provider } from 'react-redux';
import '../node_modules/@fortawesome/fontawesome-free/css/fontawesome.css';
import '../node_modules/@fortawesome/fontawesome-free/css/solid.css';
import '../node_modules/bulma/css/bulma.css';
import { AppRouter } from './AppRouter';
import './index.css';
import reportWebVitals from './reportWebVitals';
import { InitGameList } from './services/SCM';
import { store } from './store/store';

InitGameList()

render(
  <Provider store={store}>
    <AppRouter />
  </Provider>,
  document.getElementById('root')
);
reportWebVitals();
