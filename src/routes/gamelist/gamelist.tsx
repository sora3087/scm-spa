import { Component } from 'react';
import { connect, ConnectedProps } from 'react-redux';
import { useParams } from 'react-router';
import { GameList } from '../../components/GameList/GameList';
import { RootState } from '../../store/store';
import { Selector } from './selector';

const mapStateToProps = (state: RootState) => {
  return {
    games: state.app.gamesByLetter
  }
}

const connector = connect(
  mapStateToProps
)

type Props = {
  params: {
    selected: string
  }
} & ConnectedProps<typeof connector>

class GameListPage extends Component<Props> {
  public render() {
    const { games } = this.props
    const selected = this.props.params.selected || '#'
    return <div>
      <Selector
        options={Object.keys(games).map(key => ({key, label: key}))}
      />
      {
        games[selected] &&
        <GameList games={games[selected]} />
      }
    </div>
  }
}

export const ConnectedGameListPage = connector(GameListPage)

export const NavigatedConnectedGameListPage = (props: any) => {
  return <ConnectedGameListPage {...props} params={useParams()}/>
}