import { Component } from 'react';
import { Link } from 'react-router-dom';
import "./selector.scss";

type Props = {
  options: { label: string, key: string }[]
}

export class Selector extends Component<Props> {
  render () {
    return (
      <div className="game-selector">
        {this.props.options.map(
          (opt, id) =>
          <Link to={opt.key} key={id}>{opt.label.toUpperCase()}</Link>
        )}
      </div>
    )
  }
}