import { Component } from 'react';

export class NotFound extends Component {
  render() {
    return (
      <h1 className="title">Page not found</h1>
    )
  }
}