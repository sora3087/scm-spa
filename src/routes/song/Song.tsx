import { chunk } from 'lodash';
import { Component } from 'react';
import { Link, useParams } from 'react-router-dom';
import { ConnectedCollectButton } from '../../components/CollectButton';
import { DownloadButton, StreamType } from '../../components/DownloadButton';
import { ConnectedPlayButton } from '../../components/PlayButton';
import { STREAM_TYPES } from '../../constants';
import { SCM, SongResponse } from '../../services/SCM';
import { formatBytes } from '../../utils/formatBytes';
import { formatSeconds } from '../../utils/formatSeconds';

type Props = {
  params: {
    songId: string
  }
}

type State = {
  song: SongResponse
}

export class SongPage extends Component<Props, State> {
  constructor(props: any) {
    super(props)
    this.state = {
      song: {
        name: "",
        game_name: "",
        game_banner_exists: 0,
        game_id: -1,
        theme_type: "",
        uploader: "",
        downloads: "",
        loop_type: "",
        available: 0,
        length: "",
        size: "",
        start_loop_point: "",
        end_loop_point: "",
        sample_rate: ""
      }
    }
  }
  componentDidMount() {
    SCM.getSong(parseInt(this.props.params.songId))
    .subscribe((song) => {
      this.setState({ song })
    })
  }
  render() {
    const {
      name, uploader,
      game_name, game_id,
      length, size, downloads,
      start_loop_point, end_loop_point, loop_type
    } = this.state.song

    const songId = parseInt(this.props.params.songId)

    return (<>
      <h1 className="title">{name}</h1>
      <div className="columns">
        <div className="column"></div>
        <div className="column is-narrow">
          <ConnectedPlayButton songId={songId} text="Preview" className="is-primary"/>
        </div>
        <div className="column is-narrow">
          <ConnectedCollectButton songId={songId} name={name} text="Collect" className="is-success"/>
        </div>
      </div>
      <div className="columns">
        <div className="column">
          <table className="table is-striped is-fullwidth has-text-right">
            <tbody>
              <tr>
                <td>Game:</td>
                <td><Link to={`/game/${game_id}`}>{game_name}</Link></td>
              </tr>
              <tr>
                <td>Song ID:</td>
                <td>{songId}</td>
              </tr>
              <tr>
                <td>Uploader:</td>
                <td>{uploader}</td>
              </tr>
              <tr>
                <td>Upload Date:</td>
                <td></td>
              </tr>
              <tr>
                <td>Length:</td>
                <td>{formatSeconds(parseInt(length))}</td>
              </tr>
              <tr>
                <td>BRSTM Size:</td>
                <td>{formatBytes(parseInt(size))}</td>
              </tr>
              <tr>
                <td>Downloads:</td>
                <td>{downloads}</td>
              </tr>
              <tr>
                <td>Loop Type:</td>
                <td>{loop_type}</td>
              </tr>
              <tr>
                <td>Start Loop Point:</td>
                <td>{start_loop_point}</td>
              </tr>
              <tr>
                <td>End Loop Point:</td>
                <td>{end_loop_point}</td>
              </tr>
            </tbody>
          </table>
        </div>
        <div className="column">
          <table className="table is-striped is-fullwidth">
            <tbody>
              <tr>
                <td>
                  <h2 className="subtitle">Downloads</h2>
                </td>
                <td></td>
              </tr>
              {
                chunk(Object.keys(STREAM_TYPES).map((key, id) => {
                  const typeKey = key as StreamType;
                  return (
                    <td key={id}>
                      <DownloadButton
                        songId={songId}
                        name={name}
                        streamType={typeKey}
                        className="is-info"
                        text={STREAM_TYPES[typeKey]}
                      />
                    </td>
                  )
                }), 2).map((children, id) => (
                  <tr key={id}>{children}</tr>
                ))
              }
            </tbody>
          </table>
        </div>
      </div>
    </>)
  }
}

export const RoutedSongPage = (props: any) => {
  return <SongPage {...props} params={useParams()}/>
}