import { Component } from 'react';
import { connect, ConnectedProps } from 'react-redux';
import { useParams } from 'react-router';
import { Subject, takeUntil } from 'rxjs';
import { GameList } from '../../components/GameList/GameList';
import { ConnectedSongList } from '../../components/SongList/SongList';
import { GameListEntry, SCM, SongListEntry } from '../../services/SCM';
import { RootState } from '../../store/store';

const mapStateToProps = (state: RootState) => {
  return {
    games: state.app.games
  }
}

const connector = connect(
  mapStateToProps,
  {}
)

type Props = {
  params: {
    query: string
  }
} & ConnectedProps<typeof connector>

type State = {
  games: GameListEntry[],
  songs: SongListEntry[],
  searching: boolean
}

export class SearchPage extends Component<Props, State> {
  private rxUnsub: Subject<any>
  constructor(props: any) {
    super(props)
    this.state = {
      games: [],
      songs: [],
      searching: true
    }
    this.rxUnsub = new Subject()
  }
  componentWillUnmount() {
    this.rxUnsub.next(true)
    this.rxUnsub.complete()
  }
  componentDidMount() {
    this.search()
  }
  componentDidUpdate(prevProps: Props){
    if (
      prevProps.params.query !== this.props.params.query
      || (prevProps.games.length === 0 && this.props.games.length > 0)
    ) {
      // stop current search
      this.rxUnsub.next(true)
      this.search()
    }
  }
  search() {
    const {query} = this.props.params
    const reg = new RegExp(query, 'i')
    const games = this.props.games.filter(game => {
      return game.game_name.match(reg)
    })
    SCM.search(query)
    .pipe(
      takeUntil(this.rxUnsub)
    )
    .subscribe((results) => {
      const { songs } = results
      this.setState({songs, games, searching: false})
    })
  }
  render() {
    const {games, songs, searching} = this.state
    if (searching) {
      return <></>
    }
    return (
      <>
        { games.length === 0 && songs.length === 0 &&
          <p>Hmm... seems like there were no results</p>
        }
        {
          games.length > 0 &&
          <div className="columns">
            <div className="column">
              <h1 className="subtitle">Games</h1>
              <GameList games={games} />
            </div>
          </div>
        }
        {
          songs.length > 0 &&
          <div className="columns">
            <div className="column">
              <h1 className="subtitle">Songs</h1>
              <ConnectedSongList songs={songs} showGame={true}/>
            </div>
          </div>
        }
      </>
    )
  }
}

const ConnectedSearchPage = connector(SearchPage)

export const ConnectedSearchPageWithParams = (props: any) => {
  return <ConnectedSearchPage {...props} params={useParams()}/>
}