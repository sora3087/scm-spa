import React, { Component } from 'react';
import { useParams } from 'react-router-dom';
import { ConnectedSongList } from '../../components/SongList/SongList';
import { GameResponse, SCM } from '../../services/SCM';

type Props = {
  params: {
    gameId: string
  }
}

type State = {
  game: GameResponse
}

export class GamePage extends Component<Props, State> {
  constructor(props: any) {
    super(props)
    this.state = {
      game: {
        game_name: '',
        game_banner_exists: 0,
        track_count: 0,
        songs: []
      }
    }
  }
  componentDidMount() {
    SCM.getGame(parseInt(this.props.params.gameId, 10))
    .subscribe((game) => {
      this.setState({game})
    })
  }
  public render() {
    const {game_name} = this.state.game
    return <div>
      <h1 className="title">{game_name}</h1>
      <ConnectedSongList songs={this.state.game.songs}/>
    </div>
  }
}

export const RoutedGamePage = (props: any) => {
  const params = useParams()
  return <GamePage {...props} params={params}/>
}