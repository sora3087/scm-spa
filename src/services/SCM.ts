import { catchError, map, Observable, of } from 'rxjs';
import { setGames } from '../store/app/appSlice';
import { store } from '../store/store';
import { HttpBaseService } from './HttpBaseService';

export type GameListEntry = {
  game_id: number
  game_name: string
  song_count: number
}

export type GameListResponse = {
  game_count: number
  total_song_count: number
  games: GameListEntry[]
}

export type SongListEntry = {
  song_game_id: number
  song_id: number
  song_name: string
  song_available: 0 | 1
  song_uploader: string
  song_length: string
  song_downloads: string
  song_loop: string
}

export type GameResponse = {
  game_name: string
  game_banner_exists: 0 | 1
  track_count: number
  songs: SongListEntry[]
}

export type SongResponse = {
  name: string
  game_name: string
  game_banner_exists: 0 | 1
  game_id: number
  theme_type: string
  uploader: string
  downloads: string
  loop_type: string
  available: 0 | 1
  length: string
  size: string
  start_loop_point: string
  end_loop_point: string
  sample_rate: string
}

export type SearchResponse = {
  songs: SongListEntry[],
  games: GameListEntry[]
}

export async function InitGameList(): Promise<void> {
  SCM.getGamesList().subscribe(games => {
    store.dispatch(setGames(games))
  })
}

class SCMService extends HttpBaseService {
  getGamesList() {
    return this.http.get<GameListResponse>('/gamelist/')
    .pipe(
      map(resp => resp.games),
      catchError(() => of([]))
    )
  }
  getGame(id: number): Observable<GameResponse> {
    return this.http.get<GameResponse>(`/game/${id}`)
    .pipe(
      catchError(() => of({
        game_name: '',
        game_banner_exists: 0,
        track_count: 0,
        songs: []
      } as GameResponse))
    )
  }
  getSongsByGameId(id: number): Observable<SongListEntry[]> {
    return this.getGame(id).pipe(
      map(resp => resp.songs),
      catchError(() => of([]))
    )
  }
  getSong(id: number): Observable<SongResponse> {
    return this.http.get<SongResponse>(`/song/${id}`)
  }
  search(search: string): Observable<SearchResponse> {
    return this.http.get<SearchResponse>('/search/', {
      params: {
        search
      }
    })
    .pipe(
      map(resp => {
        return {
          games: resp.games || [],
          songs: resp.songs || [],
        }
      }),
      catchError(() => of({
        games: [],
        songs: []
      }))
    )
  }
}

export const SCM = new SCMService()