import { HttpClient } from "../classes/HttpClient";
import { API_PATH, ORIGIN } from '../constants';
// import Constants from 'expo-constants'

export const http = new HttpClient(`${ORIGIN}${API_PATH}`)
