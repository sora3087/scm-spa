import { connect, ConnectedProps } from 'react-redux';
import { addSong, removeSong } from '../store/collection/collectionSlice';
import { RootState } from '../store/store';
import { BulmaButton, BulmaButtonProps } from './BulmaButton';

const mapStateToProps = (state: RootState, props: BulmaButtonProps) => {
  return {
    selected: state.collection.songs.find((song) => song.id === props.songId )
  }
}

const connector = connect(
  mapStateToProps,
  {
    addSong,
    removeSong
  }
)

export class CollectButton extends BulmaButton<ConnectedProps<typeof connector>> {
  render(): JSX.Element {
    let className = "button has-fill "
    if(this.props.className) {
      className += this.props.className
    }
    if(this.props.selected) {
      className += " is-danger"
    }
    return (
      <button className={className} onClick={() => this.click()}>
        {this.content}
      </button>
    )
  }
  click() {
    if(this.props.selected) {
      this.props.removeSong(this.props.songId)
    } else {
      this.props.addSong({id: this.props.songId, name: this.props.name })
    }
  }
  get icon() {
    const { selected } = this.props
    return <i className={`fas fa-${selected?'times':'plus'}`}></i>
  }
}

export const ConnectedCollectButton = connector(CollectButton)