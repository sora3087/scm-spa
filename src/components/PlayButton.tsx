import { connect, ConnectedProps } from 'react-redux';
import { loadSong, toggle } from '../store/player/playerSlice';
import { RootState } from '../store/store';
import { BulmaButton } from './BulmaButton';

const mapStateToProps = (state: RootState, props: any) => {
  return {
    playerId: state.player.id,
    playing: state.player.playing && (props.songId === state.player.id)
  }
}

const connector = connect(
  mapStateToProps,
  {
    toggle,
    loadSong
  }
)

class PlayButton extends BulmaButton<ConnectedProps<typeof connector>> {
  get icon() {
    return (this.props.playing)
      ? <i className="fas fa-pause" />
      : <i className="fas fa-play" />
  }
  click() {
    if(this.props.playerId !== this.props.songId) {
      this.props.loadSong(this.props.songId)
    } else {
      this.props.toggle()
    }
  }
}

export const ConnectedPlayButton = connector(PlayButton)