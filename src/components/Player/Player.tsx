import { Brstm } from 'brstm';
import { Component } from 'react';
import { connect, ConnectedProps } from 'react-redux';
import { getSongBuffer } from '../../store/player/playerSlice';
import { RootState } from '../../store/store';

import "./Player.scss"

const mapStateToProps = (state: RootState) => {
  return state.player
}

const connector = connect(
  mapStateToProps,
  {}
)

type Props = ConnectedProps<typeof connector>

type State = {
  played: number
}

class Player extends Component<Props, State> {
  private brstm: Brstm | undefined
  state = { played: 0 }
  render() {
    const { loaded, id, song } = this.props
    const { played } = this.state
    return (
      <div id="playerWrapper">
        <div
          id="player"
          className={`panel ${(id > -1)?'':'hidden'}`}
        >
          <div className="close">
            <i className="fas fa-times" />
          </div>
          <div className="song-title">{song.name}</div>
          <div className="bar-wrap">
            <progress className="progress loaded" value={loaded} />
            <progress className="progress is-success" value={played} />
            <div className="times">
              <div className="time played">0:00</div>
              <div className="time remaining">0:00</div>
            </div>
          </div>
          <div className="controls">
            <div className="play-control">
              <i className="fas fa-play"></i>
            </div>
          </div>
        </div>
      </div>
    )
  }
  componentDidUpdate(prevProps: Props) {
    if(prevProps.loaded !== this.props.loaded) { 
      // loaded amount changed
      const buffer = getSongBuffer()
      if(buffer.byteLength > 64) { //64 bytes is the lenght of a BRSTM header
        this.brstm = new Brstm(buffer)
      }
    }
  }
}

export const ConnectedPlayer = connector(Player)