import { Component } from 'react';
import { useNavigate, NavigateFunction, Link } from 'react-router-dom';
import { ConnectedCollectButton } from '../../components/CollectButton';
import { ConnectedPlayButton } from '../../components/PlayButton';
import { SongListEntry } from '../../services/SCM';
import { formatSeconds } from '../../utils/formatSeconds';

type Props = {
  navigate: NavigateFunction,
  showGame?: boolean
  gameName?: string
} & SongListEntry

class SongListItem extends Component<Props> {
  render() {
    const {
      song_id, song_name, song_uploader, song_game_id,
      song_length, song_downloads, song_loop, gameName, showGame
    } = this.props

    return (
      <tr>
        <td
          onClick={() => this.navigateSong(song_id)}
          style={{cursor: 'pointer'}}
        >{song_name}</td>
        {showGame &&
          <td className="is-hidden-touch"><Link to={`/game/${song_game_id}`}>{gameName}</Link></td>
        }
        <td
          className="is-hidden-touch"
          onClick={() => this.navigateUploader(song_uploader)}
          style={{cursor: 'pointer'}}
        >{song_uploader}</td>
        <td>{formatSeconds(parseInt(song_length))}</td>
        <td>{song_downloads}</td>
        <td className="is-hidden-touch">{song_loop}</td>
        <td className="is-hidden-desktop">
          <div className="field">
            <div className="control">
              <ConnectedPlayButton songId={song_id} name={song_name} className="is-small"/>
            </div>
            <div className="control">
              <ConnectedCollectButton songId={song_id} name={song_name} className="is-small"/>
            </div>
          </div>
        </td>
        <td className="is-hidden-touch">
          <ConnectedPlayButton songId={song_id} name={song_name} className="is-small"/>
        </td>
        <td className="is-hidden-touch">
          <ConnectedCollectButton songId={song_id} name={song_name} className="is-small"/>
        </td>
      </tr>
    )
  }
  navigateSong(id: number) {
    this.props.navigate(`/song/${id}`)
  }
  navigateUploader(name: string) {
    this.props.navigate(`/user/profile/${name}`)
  }
}

export const SongListItemWithNavigate = (props: any) => {
  return <SongListItem {...props} navigate={useNavigate()} />
}