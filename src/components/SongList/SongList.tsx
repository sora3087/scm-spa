import { Component } from 'react';
import { connect, ConnectedProps } from 'react-redux';
import { SongListEntry } from '../../services/SCM';
import { RootState } from '../../store/store';
import { SongListItemWithNavigate } from './SongListItem';

type Props = {
  songs: SongListEntry[],
  showGame?: boolean
}

const connector = connect(
  (state: RootState, props: Props) => {
    return {
      games: props.showGame ? state.app.games : []
    }
  }
)


export class SongList extends Component<Props & ConnectedProps<typeof connector>> {
  render() {
    const showGame = this.props.showGame && this.props.games.length > 0
    return (
      <table className="table song-list is-striped is-fullwidth is-hoverable">
        <thead>
          <tr>
            <th>Song Name</th>
            { showGame && 
              <th className="is-hidden-touch">Game Name</th>
            }
            <th className="is-hidden-touch">Uploader</th>
            <th className="is-narrow"><span className="is-hidden-touch">Length</span><span className="is-hidden-desktop"><i className="fas fa-clock"></i></span></th>
            <th className="is-narrow"><span className="is-hidden-touch">Downloads</span><span className="is-hidden-desktop"><i className="fas fa-download"></i></span></th>
            <th className="is-narrow is-hidden-touch">Loop type</th>
            <th className="is-narrow is-hidden-desktop"></th>
            <th className="is-narrow is-hidden-touch">Preview</th>
            <th className="is-narrow is-hidden-touch">Collect</th>
          </tr>
        </thead>
        <tbody>
          {
            this.props.songs.map(
              (song, id) =>
                <SongListItemWithNavigate
                  key={id}
                  showGame={showGame}
                  gameName={showGame ? this.getGameName(song.song_game_id): null}
                  {...song}
                />
            )
          }
        </tbody>
      </table>
    )
  }
  private getGameName(id: number){
    const games = this.props.games.filter(game => game.game_id === id)
    if(games.length > 0) return games[0].game_name;
    return ''
  }
}

export const ConnectedSongList = connector(SongList)