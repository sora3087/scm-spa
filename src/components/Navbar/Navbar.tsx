import { Component } from 'react';
import { Link } from 'react-router-dom';
import { SearchBoxWithNavigation } from './SearchBox';

import "./Navbar.scss"

type State = {
  active: boolean
}

export class Navbar extends Component<{},State> {
  state = {active: false}
  private toggle(): void {
    this.setState({ active: !this.state.active })
  }
  public render(): JSX.Element {
    const isActive = (this.state.active) ? "is-active" : ""
    return (
      <nav className="navbar" role="navigation" aria-label="main navigation">
        <div className="navbar-brand">
          <h1 className="logo">SCM</h1>

          <button
            className={`navbar-burger ${isActive}`}
            aria-label="menu"
            aria-expanded="false"
            onClick={() => this.toggle()}
          >
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
          </button>
        </div>

        <div id="headerNavbar" className={`navbar-menu ${isActive}`}>
          <div className="navbar-start">
            <Link to="/" className="navbar-item">Home</Link>
            {/* <Link to="/" className="navbar-item">Random</Link> */}
          </div>

          <div className="navbar-end">
            <SearchBoxWithNavigation />
          </div>
        </div>
      </nav>
    )
  }
}