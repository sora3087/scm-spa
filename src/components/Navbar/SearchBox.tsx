import { ChangeEvent, Component, FormEvent } from 'react';
import { useNavigate, NavigateFunction } from 'react-router-dom';

type Props = {
  navigate: NavigateFunction
}

type State = {
  query: string
}

class SearchBox extends Component<Props, State> {
  constructor(props: any) {
    super(props)
    this.state = {query: ''}
  }
  render() {
    return (
      <div className="navbar-item">
        <form 
          onSubmit={(e) => this.submit(e)}
        >
          <p className="control has-icons-right">
            <input
              className="input"
              type="text"
              placeholder="Search game or song name"
              value={this.state.query}
              onChange={this.handleChange}
            />
            <span
              className="icon is-small is-right"
              onClick={() => this.submit()}
              style={{cursor: 'pointer'}}
            >
              <i className="fas fa-search"></i>
            </span>
          </p>
        </form>
      </div>
    )
  }
  handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    this.setState({query: e.target.value})
  }
  submit(event?: FormEvent) {
    if(event) event.preventDefault(); // handle if the user presses enter
    const {query} = this.state
    if (query !== '') {
      this.props.navigate(`/search/${query}`)
    }
  }
}

export const SearchBoxWithNavigation = (props: any) => {
  return <SearchBox {...props} navigate={useNavigate()}/>
}