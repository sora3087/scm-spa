import { Component } from 'react';

import './BulmaButton.scss'

export type BulmaButtonProps = {
  songId: number
  name: string
  text?: string
  className?: string
}

type State = {
  loading: boolean
  progress: number
}

export abstract class BulmaButton<T = {}> extends Component<T & BulmaButtonProps, State> {
  constructor(props: any) {
    super(props)
    this.state = { loading: false, progress: 0 }
  }
  render() {
    let className = "button has-fill "
    if(this.props.className) {
      className += this.props.className
    }
    // if(this.state.loading) {
    //   className += ' is-loading'
    // }
    return (
      <button className={className} onClick={() => this.click()}>
        {this.content}
      </button>
    )
  }
  get icon(): null | JSX.Element {
    return null
  }
  get content(): JSX.Element {
    const { text } = this.props
    const {icon} = this
    const width = this.state.progress*100 + '%'
    return <>
      <div className="fill" style={{width}}/>
      { text && <span>{text}</span>}
      {
        icon !== null
        ? (
          <span className="icon is-small">
            {icon}
          </span>
        )
        : <></>
      }
    </>
  }
  abstract click(): void
}