import JSZip from 'jszip';
import { Component } from 'react';
import { connect, ConnectedProps } from 'react-redux';
import CRC32 from '../../classes/CRC32';
import { ORIGIN, STREAM_TYPES } from '../../constants';
import { clear, removeSong } from '../../store/collection/collectionSlice';
import { RootState } from '../../store/store';
import { downloadBlob } from '../../utils/downloadBlob';
import { slugify } from '../../utils/slugify';
import { StreamType } from '../DownloadButton';
import "./Collection.scss";

const mapStateToProps = (state: RootState) => {
  return {
    ...state.collection
  }
}

const connector = connect(
  mapStateToProps,
  {
    removeSong,
    clear
  }
)

type Props = ConnectedProps<typeof connector>

type State = {
  collapsed: boolean
  downloading: boolean,
  streamType: string,
  progress: number[]
  completed: number
}

export class Collection extends Component<Props, State> {
  state = {
    collapsed: true,
    downloading: false,
    streamType: '',
    progress: [] as number[],
    completed: 0
  }
  private processList: {name: string, id: number}[] = []
  render(): JSX.Element {
    const className = [
      'card',
      this.state.collapsed ? 'collapsed' : undefined,
      this.props.songs.length < 1 ? 'empty' : undefined
    ]
    .join(' ')

    const { downloading } = this.state
    return (
      <div id="collection" className={className}>
        <header className="card-header has-background-light"  onClick={() => this.toggle()}>
          <p className="card-header-title">
            Collected songs
          </p>
          <button className="card-header-icon" aria-label="more options">
            {
              downloading
              ? this.renderDownloadCount()
              : this.renderCount()
            }
            <span className="icon">
              <i className="fas fa-list" aria-hidden="true"></i>
            </span>
          </button>
        </header>
        <div className="accordion">
          <div className="card-content">
            <div className="content">
              <table className="table is-fullwidth">
                <tbody>
                  {
                    downloading
                    ? this.renderDownloads()
                    : this.props.songs.map((song, id) => (
                      <tr key={id}>
                        <td>{song.name}</td>
                        <td><i onClick={() => this.removeSong(song.id)} className="fas fa-times"></i></td>
                      </tr>
                    ))
                  }
                </tbody>
              </table>
            </div>
          </div>
          <div className="card">
            <footer className="card-footer is-flex-direction-column">
              <div className="field">
                <div className="control">
                  <div className="select">
                    <select value={this.state.streamType} onChange={event => this.setState({ streamType: event.target.value })}>
                      <option>Select format</option>
                      {
                        Object.keys(STREAM_TYPES).map((key, id) => {
                          const typeKey = key as StreamType;
                          return (
                            <option key={id} value={typeKey}>{STREAM_TYPES[typeKey]}</option>
                          )
                        })
                      }
                    </select>
                  </div>
                </div>
              </div>
              <div className="field">
                <div className="control">
                  <button
                    onClick={() => this.download()}
                    className="button is-success is-fullwidth"
                  >Download</button>
                </div>
              </div>
            </footer>
          </div>
        </div>
      </div>
    )
  }
  renderCount(): JSX.Element {
    if(this.props.songs.length === 0) return <></>
    return <div className="tag is-info">
      {this.props.songs.length}
    </div>
  }
  renderDownloadCount(): JSX.Element {
    return <div className="tag is-success">
      {this.state.completed}/{this.state.progress.length}
    </div>
  }
  renderDownloads(): JSX.Element[] {
    const {progress} = this.state
    return this.processList.map((status, id) => (
      <tr key={id}>
        <td className="fill-wrap">
          <div className="fill" style={{ width: progress[id]*100 + "%" }}></div>
          <div className="is-flex is-justify-content-space-between">
            <span>{status.name}</span>
            <span>{(progress[id]*100).toFixed(2) + "%"}</span>
          </div>
        </td>
      </tr>
    ))
  }
  removeSong(id: number) {
    this.props.removeSong(id)
  }
  download() {
    if(
      !this.state.downloading
      && this.state.streamType !== ''
      && this.props.songs.length > 0
    ){
      this.processList = [...this.props.songs]
      this.setState({ downloading: true, progress: this.processList.map(() => 0) })
      this.props.clear()
      this.processDownloads()
    }
  }
  async processDownloads() {
    const zip = new JSZip()
    for (let i = 0; i < this.processList.length; i++) {
      const blob = await this.downloadBlob(i)
      zip.file(`${slugify(this.processList[i].name)}.${this.state.streamType}`, blob)
    }
    await zip.generateAsync({ type: 'blob' })
    .then(async (content) => {
      const hash = CRC32.hash(new Uint8Array(await content.arrayBuffer()))
      downloadBlob(content, `SCM-Collection-${hash.toString(16)}.zip`)
    })
    this.setState({ downloading: false, progress: [], collapsed: true, completed: 0 })
    this.processList = []
  }
  async downloadBlob(i: number): Promise<Blob> {
    const status = this.processList[i]
    return new Promise((resolve, reject) => {
      const xhr = new XMLHttpRequest()
      xhr.open('GET', `${ORIGIN}/${this.state.streamType}/${status.id}`)
      xhr.onprogress = (progressEvent) => {
        const progress = [...this.state.progress]
        progress[i] = progressEvent.loaded/progressEvent.total
        this.setState({ progress })
      }
      xhr.onloadend = () => {
        this.setState({ completed: this.state.completed + 1 })
        resolve(xhr.response)
      }
      xhr.responseType = "blob"
      xhr.send(null)
    })
  }
  toggle() {
    this.setState({ collapsed: !this.state.collapsed })
  }
}

export const ConnectedCollection = connector(Collection)
