import axios from 'axios';
import { ORIGIN, STREAM_TYPES } from '../constants';
import { downloadBlob } from '../utils/downloadBlob';
import { slugify } from '../utils/slugify';
import { BulmaButton } from './BulmaButton';

const contentReg = /filename="([^"]+)"$/

export type StreamType = keyof typeof STREAM_TYPES

type Props = {
  streamType: StreamType
  newTab?: boolean
}

export class DownloadButton extends BulmaButton<Props> {
  private cache: Blob = new Blob()
  private fileName: string = ''
  componentDidUpdate(){
    if(this.props.name){
      this.fileName = `${slugify(this.props.name)}.${this.props.streamType}`
    }
  }
  click() {
    const URL = `${ORIGIN}/${this.props.streamType}/${this.props.songId}`
    if(this.props.newTab) {
      window.open(
        URL,
        '_blank'
      );
    }
    if (this.cache.size !== 0){
      downloadBlob(this.cache, this.fileName)
      return
    }
    if(!this.state.loading) {
      this.setState({loading: true})
      axios.get<Blob>(URL, {
        onDownloadProgress: (progress: ProgressEvent) => {
          if(progress.total) {
            this.setState({progress: progress.loaded / progress.total})
          }
        },
        responseType: 'blob'
      })
      .then((resp) => {
        this.cache = resp.data
        if (this.fileName === '') {
          const matches = contentReg.exec(resp.headers['content'])
          if(matches) {
            this.fileName = matches[1]
          }  else {
            this.fileName = `${this.props.songId}.${this.props.streamType}`
          }
        }
        downloadBlob(resp.data, this.fileName)
      })
      .catch(() => {
        // failed to download
      })
      .finally(() => {
        this.setState({loading: false, progress: 0})
      })
    }
  }
  get icon() {
    return <i className={`fas ${this.state.loading ? 'fa-circle-notch fa-spin' : 'fa-arrow-down'}`}></i>
  }
}