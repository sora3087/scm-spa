import { Component } from 'react'
import { useNavigate, NavigateFunction } from 'react-router-dom';
import { GameListEntry } from '../../services/SCM';

type Props = {
  navigate: NavigateFunction
} & GameListEntry

class GameListItem extends Component<Props> {
  render() {
    const { game_name, song_count } = this.props
    return (
      <tr onClick={() => this.handleClick()} style={{cursor: 'pointer'}}>
        <td>{game_name}</td>
        <td>{song_count}</td>
      </tr>
    )
  }
  handleClick() {
    this.props.navigate(`/game/${this.props.game_id}`)
  }
}

export const GameListItemWithNavigate = (props: any) => {
  return <GameListItem {...props} navigate={useNavigate()}/>
}