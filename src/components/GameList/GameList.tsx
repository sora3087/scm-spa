import { Component } from 'react';
import { GameListEntry } from '../../services/SCM';
import { GameListItemWithNavigate } from './GameListItem';

type Props = {
  games: GameListEntry[]
}

export class GameList extends Component<Props> {
  render() {
    return (
      <table className="table is-striped is-fullwidth is-hoverable">
        <thead>
          <tr>
            <th>
              Game
            </th>
            <th>
              Songs
            </th>
          </tr>
        </thead>
        <tbody>
          {
            this.props.games.map(
              (game, id) => <GameListItemWithNavigate key={id} {...game}/>
            )
          }
        </tbody>
      </table>
    )
  }
}