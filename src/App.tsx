import { Outlet } from 'react-router';
import { ConnectedCollection } from './components/Collection/Collection';
import { Navbar } from './components/Navbar/Navbar';
import { ConnectedPlayer } from './components/Player/Player';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Navbar />
      </header>
      <div className="container">
        <Outlet/>
      </div>
      <ConnectedPlayer />
      <ConnectedCollection />
    </div>
  );
}

export default App;
